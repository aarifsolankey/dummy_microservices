const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const User = db.user;

verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];
  if (!token) {
    return res.status(403).send({
      message: "No token provided!"
    });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "Unauthorized!"
      });
    }
    req.userId = decoded.id;
    next();
  });
};

isAdmin = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getProfiles().then(profiles => {
      for (let i = 0; i < profiles.length; i++) {
        if (profiles[i].pid === 111) {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "Require Admin Profile!"
      });
      return;
    });
  });
};

isAgent = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getProfiles().then(profiles => {
      for (let i = 0; i < profiles.length; i++) {
        if (profiles[i].pid === 222) {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "Require Agent Profile!"
      });
    });
  });
};

isRetailer = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getProfiles().then(profiles => {
      for (let i = 0; i < profiles.length; i++) {
        if (profiles[i].pid === 555) {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "Require Retailer Profile!"
      });
    });
  });
};

isSales = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getProfiles().then(profiles => {
      for (let i = 0; i < profiles.length; i++) {
        if (profiles[i].pid === 333) {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "Require Sales Profile!"
      });
    });
  });
};

isDistributor = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getProfiles().then(profiles => {
      for (let i = 0; i < profiles.length; i++) {
        if (profiles[i].pid === 444) {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "Require Distributor Profile!"
      });
    });
  });
};


isManager = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getProfiles().then(profiles => {
      for (let i = 0; i < profiles.length; i++) {
        if (profiles[i].pid === 666) {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "Require Distributor Profile!"
      });
    });
  });
};

isLead = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getProfiles().then(profiles => {
      for (let i = 0; i < profiles.length; i++) {
        if (profiles[i].pid === 777) {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "Require Lead Profile!"
      });
    });
  });
};

isClerk = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getProfiles().then(profiles => {
      for (let i = 0; i < profiles.length; i++) {
        if (profiles[i].pid === 888) {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "Require Clerk Profile!"
      });
    });
  });
};


isMystry = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getProfiles().then(profiles => {
      for (let i = 0; i < profiles.length; i++) {
        if (profiles[i].pid === 999) {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "Require Mystry Profile!"
      });
    });
  });
};


isManagerOrAdmin = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getProfiles().then(profiles => {
      for (let i = 0; i < profiles.length; i++) {
        if (profiles[i].pid === 666) {
          next();
          return;
        }

        if (profiles[i].pid === 111) {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "Require Manager or Admin Profile!"
      });
    });
  });
};

const authJwt = {
  verifyToken: verifyToken,
  isAdmin: isAdmin,
  isAgent: isAgent,
  isRetailer: isRetailer,
  isSales: isSales,
  isDistributor: isDistributor,
  isManager: isManager,
  isLead: isLead,
  isClerk: isClerk,
  isMystry: isMystry,
  isManagerOrAdmin: isManagerOrAdmin
};
module.exports = authJwt;