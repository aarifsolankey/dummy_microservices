const multer = require("multer");

const excelFilter = (req, file, cb) => {
  const fileType = file.mimetype;
  if (fileType.includes("jpeg") || fileType.includes("png") || 
    fileType.includes("jpg") || fileType.includes("svg")
  ) {
    cb(null, true);
  } else {
    cb("Please upload only image.", false);
  }
};

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "enroll_images/");
  },
  filename: (req, file, cb) => {
    console.log(file.originalname);
    cb(null, `image-${Date.now()}`);
  },
});

var uploadFile = multer({ storage: storage, fileFilter: excelFilter });
module.exports = uploadFile;