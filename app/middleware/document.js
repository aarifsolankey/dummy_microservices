const multer = require("multer");
var path = require('path');

const excelFilter = (req, file, cb) => {
  const fileType = file.mimetype;
  if (fileType.includes("jpeg") || fileType.includes("png") || 
    fileType.includes("jpg") || fileType.includes("svg")
  ) {
    cb(null, true);
  } else {
    cb("Please upload only image.", false);
  }
};

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "enroll_images/");
  },
  filename: (req, file, cb) => {
    cb(null, `image-${Date.now()}${path.extname(file.originalname)}`);
  },
});

var uploadFile = multer({ storage: storage, fileFilter: excelFilter });
module.exports = uploadFile;