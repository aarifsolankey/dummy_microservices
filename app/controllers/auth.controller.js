
const { signupService,signinService } = require('../services/auth.service');


exports.signup = async(req, res) => {
  
try{  
    const signUpStatus = await signupService(req.body)
    res.status(200).send(signUpStatus);
  }
  catch(err)
    {        
      res.status(400).send({
            message: 'Cant process request'
      });
    }
  
};

exports.signin = async(req, res) => {
    
try{  
  const signInStatus = await signinService(req.body)
  res.status(200).send(signInStatus);
}
catch(err)
  {        
    res.status(400).send({
          message: 'Cant process request'
    });
  }
};
