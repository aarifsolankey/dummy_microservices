
const { getAllService, createService, getByIdService, update, destroy } = require('../services/document.service');
const db = require("../models");
const path = require("path")
exports.getAll = async(req, res) => {
  try{  
    const data = await getAllService(req.params.programId);
    res.status(200).send(data);
  }
  catch(err)
    {        
      res.status(400).send({
            message:  'Cant process request'
      });
    }  
  };
  
  exports.findOne = async(req, res) => {
    try{  
      const data = await getByIdService(req.params.id);
      res.status(200).send({
        message: data == null ? 'Data not found.' : 'Fatched successfully ' ,
        data: data,
  });
    }
    catch(err)
      {        
        res.status(400).send({
              message: 'Cant process request'
        });
      }    
    };

  exports.create = async(req, res) => {
    try{  
      var document = []

      req.files.forEach((element,key) => {
     
        var post = {
          'title':element.filename,
          'path':path.join(__dirname, "../../enroll_images/", element.filename),
          'type':element.mimetype, 
        }
        documents =   db.document.create(post);
         document.push(documents)
    
      });
      Promise.all(document).then((values) => {
        res.status(200).send(values);
      //   response = [];
      //   values.forEach((element,key) => {
      //     // element.push()
      //     response.push({
      //       'id':element.id,
      //       'title':element.title,
      //       'path':element.path,
      //       'type':element.type, 
      //       'createdAt':element.createdAt, 
      //       'updatedAt':element.updatedAt, 
      //       'doc_type':req.body.type, 
      //     })
      //   })
      //   res.status(200).send(response);
      });
    }
    catch(err)
      {        
        res.status(400).send({
              message: 'Cant process request'
        });
      }    
    };
  
  exports.edit = async(req, res) => {
    try{  
      const data = await update(req.params.id,req.body);
      res.status(200).send({
        message: data == 1 ? 'Updated successfully' : 'data not found with this id',
  });
    }
    catch(err)
      {        
        res.status(400).send({
              message: 'Cant process request'
        });
      }    
  };
  
  exports.delete = async(req, res) => {
    try{  
      const data = await destroy(req.params.id,req.body);
      res.status(200).send({
        message: data == 1 ? 'Deleted successfully' : 'data not found with this id',
  });
    }
    catch(err)
      {        
        res.status(400).send({
              message: 'Cant process request'
        });
      }    

  }