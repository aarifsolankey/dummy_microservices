
const { getAllService, createService, getByIdService, update, destroy } = require('../services/brandCategory.service');

exports.getAll = async(req, res) => {
  try{  
    const data = await getAllService(req.params.programId);
    res.status(200).send(data);
  }
  catch(err)
    {        
      res.status(400).send({
            message:  'Cant process request'
      });
    }  
  };
  
  exports.findOne = async(req, res) => {
    try{  
      const data = await getByIdService(req.params.id);
      res.status(200).send({
        message: data == null ? 'Data not found.' : 'Fatched successfully ' ,
        data: data,
  });
    }
    catch(err)
      {        
        res.status(400).send({
              message: 'Cant process request'
        });
      }    
    };

  exports.create = async(req, res) => {
    try{  
      const allPrgram = await createService(req.body)
      res.status(200).send(allPrgram);
    }
    catch(err)
      {        
        res.status(400).send({
              message: 'Cant process request'
        });
      }    
    };
  
  exports.edit = async(req, res) => {
    try{  
      const data = await update(req.params.id,req.body);
      res.status(200).send({
        message: data == 1 ? 'Updated successfully' : 'data not found with this id',
  });
    }
    catch(err)
      {        
        res.status(400).send({
              message: 'Cant process request'
        });
      }    
  };
  
  exports.delete = async(req, res) => {
    try{  
      const data = await destroy(req.params.id,req.body);
      res.status(200).send({
        message: data == 1 ? 'Deleted successfully' : 'data not found with this id',
  });
    }
    catch(err)
      {        
        res.status(400).send({
              message: 'Cant process request'
        });
      }    

  }