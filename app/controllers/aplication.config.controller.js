
const { crud } = require('../../crud/services/crudService');
const { MasterSetupService,DeleteMasterSetupService } = require('../services/master-setup.service');
const { ExcelUpload } = require('../services/excelUpload.service');
const { getAllStatus } = require('../services/configration.service');
const db = require('../models/index');

exports.CreateCrud = (req, res) => {
  try{  
    const cruds = crud(req);
  res.status(200).send(cruds);
  }
  catch(err)
    {        
      res.status(400).send({
            message:  'Cant process request'
      });
    } 
  };

  exports.DeleteMasterSetup = async(req, res) => {
    const data = await DeleteMasterSetupService(req.params.program_id);
        res.status(200).send({
          "status":true,
          "msg":"Master setup deleted successfully",
          "data":data,
        }); 
  }
  
    exports.allStatus = async(req, res) => {
      try{ 
        const service = await getAllStatus(req);
        res.send(service);
      }
      catch(err)
        {        
          res.send({
                message: err
          });
        } 
    }
      exports.MasterSetup = async(req, res) => {
    
    try{ 
      var date = new Date();
      var checkProgram = await db.program.findOne({where:{status:true}}) 
      if(!checkProgram){
        formData = {
          "name":"Cadbury",
          "startDate":date,
          "endDate":date,
          "currency":"INR",
          "status":1,
        }
        var program = await db.program.create(formData) 
        if(program){
          const data = await MasterSetupService(program.id);
        res.status(200).send({
          "status":true,
          "msg":"Master setup done",
          "data":data,
        }); 
        }else{
          res.status(400).send({
            "status":false,
            "msg":"Something wrong with request.",
            "data":[],
          }); 
        }
      }else{
        res.status(400).send({
          "status":false,
          "msg":"Program already exist",
          "data":checkProgram,
        }); 
      }
    }
    catch(err)
      {        
        res.send({
              message: err
        });
      } 
    };
    

    // exports.excelUpload = (req, res) => {
    
    //   // try{  
    //     const data =  ExcelUpload(req.params.program_id);
    //     res.status(200).send({
    //       "status":true,
    //       "msg":"dfas",
    //       "data":data,
    //     });
    //   // }
    //   // catch(err)
    //   //   {        
    //   //     res.send({
    //   //           message: err
    //   //     });
    //   //   } 
    //   };
  
    exports.excelUpload = async (req, res) => {
      const modelName = db.user

      let fields = ["fields","title","description","published"]
      let defaultFields = {"program_id":1,"dd":2}
       var data = await ExcelUpload(req,modelName,fields,defaultFields);
       res.send(data)
      };
      