module.exports = app => {
    const controller = require("../controllers/document.controller.js");
    var router = require("express").Router();
    const { authJwt } = require("../middleware");
    const upload = require("../middleware/document");

    router.post("/",[authJwt.verifyToken],upload.array("file",10), controller.create);
    router.get("/list/:programId",[authJwt.verifyToken, authJwt.isAdmin], controller.getAll);
    router.get("/:id",[authJwt.verifyToken],controller.findOne);
    router.put("/:id",[authJwt.verifyToken],controller.edit);
    router.delete("/:id",[authJwt.verifyToken, authJwt.isAdmin],  controller.delete);
    app.use('/api/document', router);
};