module.exports = app => {
    const controller = require("../controllers/userdetail.controller.js");
    var router = require("express").Router();
    const { authJwt } = require("../middleware");

    router.post("/", controller.create);
    router.get("/list/:programId",[authJwt.verifyToken, authJwt.isAdmin], controller.getAll);
    router.get("/:id", controller.findOne);
    router.put("/:id", controller.edit);
    router.delete("/:id", controller.delete);
    app.use('/api/userdetail', router);
};