module.exports = app => {
    const controller = require("../controllers/frequency.controller.js");
    const { authJwt } = require("../middleware");

    var router = require("express").Router();
    router.post("/",[authJwt.verifyToken, authJwt.isAdmin], controller.create);
    router.get("/list/:programId",[authJwt.verifyToken, authJwt.isAdmin], controller.getAll);
    router.get("/:id",[authJwt.verifyToken, authJwt.isAdmin], controller.findOne);
    router.put("/:id",[authJwt.verifyToken, authJwt.isAdmin], controller.edit);
    router.delete("/:id",[authJwt.verifyToken, authJwt.isAdmin], controller.delete);
    app.use('/api/frequency', router);
};