module.exports = app => {
    const controller = require("../controllers/enrollment.controller.js");
    var router = require("express").Router();
    const {validation} = require('../services/enrollment.service');
    const upload = require("../middleware/upload_image");
    const { authJwt } = require("../middleware");
    router.post("/",[authJwt.verifyToken],[validation],controller.create);
    router.get("/list/:programId",[authJwt.verifyToken], controller.getAll);
    router.get("/:id",[authJwt.verifyToken],controller.findOne);
    router.put("/:id",[authJwt.verifyToken], upload.array("adhaar_images",2),[validation],controller.edit);
    router.put("/update-status/:id",[authJwt.verifyToken],controller.updateStatus);
    router.delete("/:id", [authJwt.verifyToken],controller.delete);
    app.use('/api/enrollment', router);
}