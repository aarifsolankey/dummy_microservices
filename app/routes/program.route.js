const { authJwt } = require("../middleware");
const controller = require("../controllers/program.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get(
    "/api/program/list",
    // [authJwt.verifyToken, authJwt.isAdmin],
    controller.getAll
  );

  app.get(
    "/api/program/single/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.getSingle
  );

  app.post(
    "/api/program/create",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.create
  );

  app.post(
    "/api/program/edit/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.edit
  );
  app.get(
    "/api/program/delete/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.delete
  );


};