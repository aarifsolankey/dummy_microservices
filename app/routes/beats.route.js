module.exports = app => {
    const controller = require("../controllers/beats.controller.js");
    var router = require("express").Router();
    const upload = require("../middleware/upload");
    const { authJwt } = require("../middleware");

    router.post("/", controller.create);
    router.post("/bulk-upload/:program_id",upload.single("file"),controller.excelUpload);
    router.get("/list/:programId",[authJwt.verifyToken, authJwt.isAdmin], controller.getAll);
    router.get("/:id",[authJwt.verifyToken, authJwt.isAdmin], controller.findOne);
    router.put("/:id",[authJwt.verifyToken, authJwt.isAdmin], controller.edit);
    router.delete("/:id",[authJwt.verifyToken, authJwt.isAdmin], controller.delete);
    app.use('/api/beats', router);
};