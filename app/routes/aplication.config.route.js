module.exports = app => {
    const controller = require("../controllers/aplication.config.controller.js");
    var router = require("express").Router();
    const upload = require("../middleware/upload");

    router.post("/crud", controller.CreateCrud);
    router.post("/master-setup", controller.MasterSetup);
    router.get("/all-status", controller.allStatus);
    router.delete("/delete-master-setup/:program_id", controller.DeleteMasterSetup);
    router.post("/upload/:program_id", upload.single("file"),controller.excelUpload);
    app.use('/api/aplication-config', router);
};