module.exports = (sequelize, DataType) => {
  const model = sequelize.define("comment", 
    {title:{type:DataType.STRING},description:{type:DataType.STRING},type:{type:DataType.STRING},program_id:{type:DataType.STRING}}
  );

  return model;
};