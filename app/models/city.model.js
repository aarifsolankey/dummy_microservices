module.exports = (sequelize, DataType) => {
  const model = sequelize.define("cities", 
    {state_id:{type:DataType.INTEGER},cityname:{type:DataType.STRING},status:{type:DataType.INTEGER},program_id:{type:DataType.INTEGER}}
  );

  return model;
};