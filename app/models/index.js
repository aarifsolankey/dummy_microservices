const config = require("../../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    operatorsAliases: false,

    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("./user.model.js")(sequelize, Sequelize);
db.profile = require("./profile.model.js")(sequelize, Sequelize);
db.program = require("./program.model.js")(sequelize, Sequelize);
db.content = require("./content.model.js")(sequelize, Sequelize);
db.language = require("./language.model.js")(sequelize, Sequelize);
db.region = require("./region.model.js")(sequelize, Sequelize);
db.state = require("./state.model.js")(sequelize, Sequelize);
db.city = require("./city.model.js")(sequelize, Sequelize);
db.channel = require("./channel.model.js")(sequelize, Sequelize);
db.brandCategory = require("./brand.category.model.js")(sequelize, Sequelize);
db.unit = require("./unit.model.js")(sequelize, Sequelize);
db.frequency = require("./frequency.model.js")(sequelize, Sequelize);
db.brand = require("./brand.model.js")(sequelize, Sequelize);
db.geographic = require("./geographic.model.js")(sequelize, Sequelize);
db.outlet = require("./outlet.model.js")(sequelize, Sequelize);
db.slab = require("./slab.model.js")(sequelize, Sequelize);
db.game = require("./game.model.js")(sequelize, Sequelize);
db.user_role = require("./user.role.model.js")(sequelize, Sequelize);
db.channel_classification = require("./channel.classification.model.js")(sequelize, Sequelize);
db.channel_classification = require("./channel.classification.model.js")(sequelize, Sequelize);
db.channel_classification = require("./channel.classification.model.js")(sequelize, Sequelize);
db.challengeReason = require("./challenge.reason.model.js")(sequelize, Sequelize);
db.beats = require("./beats.model.js")(sequelize, Sequelize);
db.UserBeatMapping = require("./user.beat.mapping.model.js")(sequelize, Sequelize);

db.userdetail = require("./userdetail.model.js")(sequelize, Sequelize);
db.userdetail = require("./userdetail.model.js")(sequelize, Sequelize);
db.enrollment = require("./enrollment.model.js")(sequelize, Sequelize);
db.document = require("./document.model.js")(sequelize, Sequelize);
db.comment = require("./comment.model.js")(sequelize, Sequelize);
db.enrollmentStatus = require("./enrollment.status.model.js")(sequelize, Sequelize);
db.enrollement_doc_mapping = require("./enrollement.doc.mapping.model.js")(sequelize, Sequelize);
// __ADD_NEW_MODEL__


db.profile.belongsToMany(db.user, {
  through: "user_profiles",
  foreignKey: "profileId",
  otherKey: "userId"
});

db.user.belongsToMany(db.profile, {
  through: "user_profiles",
  foreignKey: "userId",
  otherKey: "profileId"
});

db.enrollment.belongsToMany(db.document, {
  through: "enrollement_doc_mapping",
  foreignKey: "enrollement_id",
  otherKey: "doc_id"
});
db.state.belongsTo(db.region, { foreignKey: 'region_id', as: "region" });
db.city.belongsTo(db.state, { foreignKey: 'state_id', as: "state" });
db.enrollment.hasOne(db.enrollmentStatus, { foreignKey: "enrollment_id", as: "enroll_status" });

db.ROLES = [111,222,333,444,555,666,777,888,999];

db.content.belongsTo(db.program)


module.exports = db;