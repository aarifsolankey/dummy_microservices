module.exports = (sequelize, DataType) => {
  const model = sequelize.define("userdetail", 
    {name:{type:DataType.STRING},dob:{type:DataType.DATE},outlet_id:{type:DataType.INTEGER},mobile_no:{type:DataType.INTEGER},city_id:{type:DataType.INTEGER},adhaar_no:{type:DataType.STRING},pan_no:{type:DataType.STRING}}
  );

  return model;
};