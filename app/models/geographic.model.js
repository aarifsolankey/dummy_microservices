module.exports = (sequelize, DataType) => {
  const model = sequelize.define("geographical", 
    {geograph_code:{type:DataType.STRING},geograph_name:{type:DataType.STRING},status:{type:DataType.INTEGER},state_id:{type:DataType.INTEGER},program_id:{type:DataType.INTEGER}}
  );

  return model;
};