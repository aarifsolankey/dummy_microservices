module.exports = (sequelize, DataType) => {
  const model = sequelize.define("slabs", 
    {slab:{type:DataType.STRING},slab_description:{type:DataType.STRING},status:{type:DataType.INTEGER},program_id:{type:DataType.INTEGER}}
  );

  return model;
};