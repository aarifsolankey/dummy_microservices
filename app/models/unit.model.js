module.exports = (sequelize, DataType) => {
  const model = sequelize.define("units", 
    {unit_name:{type:DataType.STRING},unit_desc:{type:DataType.STRING},status:{type:DataType.INTEGER},unit_value:{type:DataType.INTEGER},program_id:{type:DataType.INTEGER}}
  );

  return model;
};