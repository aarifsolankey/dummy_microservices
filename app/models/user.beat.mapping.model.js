module.exports = (sequelize, DataType) => {
  const model = sequelize.define("user_beat_mapping", 
    {
      beat_id:{type:DataType.STRING},
      user_id:{type:DataType.INTEGER},
      status:{type:DataType.INTEGER}}
  );

  return model;
};