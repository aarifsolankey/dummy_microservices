module.exports = (sequelize, DataType) => {
  const model = sequelize.define("enrollment_status", 
    {
      enrollment_id:{
        type:DataType.INTEGER
      },
      enrollment_status:{
        type:DataType.INTEGER
      },
      description:{
        type:DataType.STRING
      },
      status:{
        type:DataType.INTEGER
      }
    },
    {
      tableName: 'enrollment_status'
    }
  );

  return model;
};