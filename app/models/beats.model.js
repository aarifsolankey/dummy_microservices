module.exports = (sequelize, DataType) => {
  const model = sequelize.define("beats", 
    {
      beat:{
        type:DataType.STRING
      },
      status:{
        type:DataType.INTEGER
      },
      program_id:{
        type:DataType.INTEGER
      }
    }
  );

  return model;
};