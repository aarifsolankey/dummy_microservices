module.exports = (sequelize, DataType) => {
  const model = sequelize.define("user_role", 
    {role_name:{type:DataType.STRING},role_level:{type:DataType.INTEGER},status:{type:DataType.INTEGER},program_id:{type:DataType.INTEGER}}
  );

  return model;
};