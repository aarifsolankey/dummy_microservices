const softDelete = require('sequelize-soft-delete')

module.exports = (sequelize, DataType) => {
  const model = sequelize.define("enrollment", 
    {
      name:{
        type:DataType.STRING
      },
      dob:{
        type:DataType.DATE
      },
      outlet_id:{
        type:DataType.INTEGER
      },
      mobile_no:{
        type:DataType.INTEGER
      },
      city_id:{
        type:DataType.INTEGER
      },
      adhaar_no:{
        type:DataType.STRING
      },
      pan_no:{
        type:DataType.STRING
      },
      program_id:{
        type:DataType.STRING
      },
      deletedAt: {
        type: DataType.INTEGER(1),
        defaultValue: '0'
      }
    },
    {
      defaultScope: {
        where: {
          deletedAt: '0'
        }
      }
    }
    
  );
  const options = {field: 'deletedAt', deleted: 1}
  softDelete.softDelete(model, options)
    return model;
};