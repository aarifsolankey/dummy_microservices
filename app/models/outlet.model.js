module.exports = (sequelize, DataType) => {
  const outlet = sequelize.define("outlets", {
      name:{type:DataType.STRING},
      user_id:{type:DataType.INTEGER},
      sort_name:{type:DataType.STRING},
      code:{type:DataType.STRING},
      email:{type:DataType.STRING},
      phone:{type:DataType.INTEGER},
      status:{type:DataType.INTEGER},
      description:{type:DataType.STRING},
      program_id:{type:DataType.INTEGER}
    }
  );
  return outlet;
};