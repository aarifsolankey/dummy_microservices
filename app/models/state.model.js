module.exports = (sequelize, DataType) => {
  const model = sequelize.define("states", 
    {
      state_code:{type:DataType.INTEGER},
      state_name:{type:DataType.STRING},
      region_id:{type:DataType.INTEGER},
      status:{type:DataType.INTEGER},
      program_id:{type:DataType.INTEGER}}
  );

  return model;
};