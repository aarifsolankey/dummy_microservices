module.exports = (sequelize, DataType) => {
  const model = sequelize.define("ducument", 
    {
      title:{
        type:DataType.STRING
      },
      path:{
        type:DataType.STRING
      },
      type:{
        type:DataType.STRING
      }
    }
  );

  return model;
};