module.exports = (sequelize, Sequelize) => {
    const Content = sequelize.define("contents", {
      title: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.INTEGER
      },
      content: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      }
    });
  
    return Content;
  };