module.exports = (sequelize, DataType) => {
  const model = sequelize.define("challenge_reasons", 
    {reason:{type:DataType.STRING},status:{type:DataType.INTEGER},program_id:{type:DataType.INTEGER}}
  );

  return model;
};