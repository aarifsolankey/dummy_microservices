module.exports = (sequelize, DataType) => {
  const model = sequelize.define("brand_categories", 
    {brand_category_name:{type:DataType.STRING},short_name:{type:DataType.STRING},crown_brand_name:{type:DataType.STRING},status:{type:DataType.INTEGER},target_threshold_percentage:{type:DataType.INTEGER},program_id:{type:DataType.INTEGER}}
  );

  return model;
};