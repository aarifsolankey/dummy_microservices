module.exports = (sequelize, DataType) => {
  const model = sequelize.define("games", 
    {name:{type:DataType.STRING},description:{type:DataType.STRING},image:{type:DataType.STRING},game_url:{type:DataType.STRING},status:{type:DataType.INTEGER},start_date:{type:DataType.DATE},end_date:{type:DataType.DATE},program_id:{type:DataType.INTEGER}}
  );

  return model;
};