module.exports = (sequelize, DataType) => {
  const model = sequelize.define("channels", 
    {channel_code:{type:DataType.STRING},channel_desc_hi:{type:DataType.STRING},channel_desc_ka:{type:DataType.STRING},channel_desc_ma:{type:DataType.STRING},channel_desc_te:{type:DataType.STRING},channel_desc_be:{type:DataType.STRING},channel_desc_ae:{type:DataType.STRING},channel_desc_ta:{type:DataType.STRING},channel_type:{type:DataType.INTEGER},parent_channel_id:{type:DataType.INTEGER},status:{type:DataType.INTEGER},hierarchy_level:{type:DataType.INTEGER},channel_classification_id:{type:DataType.INTEGER},program_id:{type:DataType.INTEGER}}
  );

  return model;
};