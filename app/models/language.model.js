module.exports = (sequelize, DataType) => {
  const model = sequelize.define("languages", 
    {
      short_name:{
        type:DataType.STRING
      },
      lang_name:{
        type:DataType.STRING
      },
      status:{
        type:DataType.INTEGER
      },
      program_id:{
        type:DataType.INTEGER
      }
    }
  );

  return model;
};