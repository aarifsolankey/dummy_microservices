module.exports = (sequelize, DataType) => {
  const model = sequelize.define("brands", 
    {brand_name:{type:DataType.STRING},sku:{type:DataType.STRING},brand_desc:{type:DataType.STRING},status:{type:DataType.INTEGER},par_brand_id:{type:DataType.INTEGER},brand_category_id:{type:DataType.INTEGER},target_threshold_percentage:{type:DataType.INTEGER},is_loyalty:{type:DataType.INTEGER},price:{type:DataType.INTEGER},unit_converstion_factor:{type:DataType.INTEGER},default_points:{type:DataType.INTEGER},total_unit_qty:{type:DataType.INTEGER},unit_per_case:{type:DataType.INTEGER},program_id:{type:DataType.INTEGER}}
  );

  return model;
};