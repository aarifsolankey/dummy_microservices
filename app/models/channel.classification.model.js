module.exports = (sequelize, DataType) => {
  const model = sequelize.define("channel_classification", 
    {classification_name:{type:DataType.STRING},status:{type:DataType.INTEGER}}
  );

  return model;
};