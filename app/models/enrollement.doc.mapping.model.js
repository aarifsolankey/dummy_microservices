module.exports = (sequelize, DataType) => {
  const model = sequelize.define("enrollement_doc_mapping", 
    {
    enrollement_id:{type:DataType.INTEGER},
    doc_id:{type:DataType.INTEGER},
    type:{type:DataType.INTEGER}
  },
  {
    tableName: 'enrollement_doc_mapping'
  }
  );

  return model;
};