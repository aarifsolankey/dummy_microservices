module.exports = (sequelize, DataType) => {
  const model = sequelize.define("regions", 
    {region_name:{type:DataType.STRING},status:{type:DataType.INTEGER},geographical_id:{type:DataType.INTEGER},program_id:{type:DataType.INTEGER}}
  );

  return model;
};