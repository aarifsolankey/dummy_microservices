module.exports = (sequelize, Sequelize) => {
    const Program = sequelize.define("programs", {
      name: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      },
      brand: {
        type: Sequelize.STRING
      },
      startDate: {
        type: Sequelize.DATE
      },
      endDate: {
        type: Sequelize.DATE
      },
      currency: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      }
    });
  
    return Program;
  };