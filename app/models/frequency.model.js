module.exports = (sequelize, DataType) => {
  const model = sequelize.define("frequencies", 
    {frequency_name:{type:DataType.STRING},status:{type:DataType.INTEGER},campaign_id:{type:DataType.INTEGER},program_id:{type:DataType.INTEGER}}
  );

  return model;
};