const db = require("../models");
const authConfig = require("../config/auth.config");
const appConfig = require("../config/app.config");

const Program = db.program;


const Op = db.Sequelize.Op;

exports.getAllService = async(programId)=>{
  console.log('Here i am-- Service getAll')

    return  new Promise ( resolve => {
      try{
         Program.findAll({
          where:{
            status:true,
          },
        }).then(programs => {
          resolve(programs)
        })
      }
        catch(err)
        {
            resolve({message: err.message });
        }
    })
}


exports.createService = async(programData)=>{
  return  new Promise(resolve => {
    try{
      Program.create({
        name: programData.name,
        brand: programData.brand,
        currency:programData.currency,
        status:true,
    }).then(programs => {
      resolve({message: "Program was creted successfully!" });              
    });
    
    }
      
    catch(err)
    {
        resolve({message: 'Input Data was not correct' });
        console.log(err.message)
    }

})
}

exports.getByIdService = async(id)=>{
  console.log('Here i am-- Service getAll')

    return  new Promise ( resolve => {
      try{
         Program.findOne({
          where:{
            id:id,
            status:true,
          },
        }).then(programs => {
          resolve(programs)
        })
      }
        catch(err)
        {
            resolve({message: err.message });
        }
    })
}

exports.update = async(id,post)=>{
  console.log('Here i am-- Service getAll')

    return  new Promise ( resolve => {
      try{
        Program.update(post, {
          where: { id: id }
        }).then(programs => {
          resolve(programs)
        })
      }
        catch(err)
        {
            resolve({message: err.message });
        }
    })
  }
    exports.destroy = async(id)=>{
      console.log('Here i am-- Service getAll')
    
        return  new Promise ( resolve => {
          try{
            Program.destroy({
              where: { id: id }
            }).then(programs => {
              resolve(programs)
            })
          }
            catch(err)
            {
                resolve({message: err.message });
            }
        })
}

