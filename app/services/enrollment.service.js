const db = require("../models");
const authConfig = require("../config/auth.config");
const appConfig = require("../config/app.config");
const { DB } = require("../../config/db.config");


const model = db.enrollment;


const Op = db.Sequelize.Op;

exports.getAllService =async(programId)=>{
  console.log('Here i am-- Service getAll')

      try{
      const data = await  model.findAll({
          where:{
            program_id:programId
          },
          include: [
            {
              attributes: ['id', 'title','path','type'],
              model: db.document,
            },
            {
              attributes: ['id', 'enrollment_status'],
              required: false,
              model: db.enrollmentStatus,
              as:'enroll_status',
              where:{
                // enrollment_status:1
              }
            }
          ],
          order: [['id', 'DESC']]
        })
        if(data.length){
          return {
            status:true,
            mst:"Data fetch successfully.",
            data:data,
          }
        }else{
          return {
            status:false,
            mst:"Data not found.",
            data:[],
          }
        }
          
      }
        catch(err)
        {
          return {
            status:false,
            mst:"Something wrong with request.",
            data:[],
          }
        }
}


exports.createService = async(req)=>{
    try{
      var enroll = await model.create(req.body)
      const adhaar = req.body.adhaar_images
      adhaar.forEach(async(element,key) => {
        mappingData = {
          'enrollement_id':enroll.id,
          'doc_id':element.id,
          'type':1,//Adhaar Images
        }
        var mapping = await db.enrollement_doc_mapping.create(mappingData)

      });
      const pan = req.body.pan_images
      pan.forEach(async(element,key) => {
        mappingData = {
          'enrollement_id':enroll.id,
          'doc_id':element.id,
          'type':2,//Pan Images
        }
        var mapping = await db.enrollement_doc_mapping.create(mappingData)

      });
          return {
            "status":true,
            "msg":"Entery was created successfully!",
            "data":enroll
          };              
    
    }
      
    catch(err)
    {
      return {
        status:false,
        mst:"Something wrong with request.",
        data:[],
      }
    }
}

exports.getByIdService = async(id)=>{
  
  try{
    const data = await  model.findOne({
        where:{
          id:id
        },
        include: [
          {
            attributes: ['id', 'title','path','type'],
            model: db.document,
          },
          {
            attributes: ['id', 'enrollment_status'],
            required: false,
            model: db.enrollmentStatus,
            as:'enroll_status',
            where:{
              // enrollment_status:1
            }
          }
        ]
      })
      if(data){
        return {
          status:true,
          mst:"Data fetch successfully.",
          data:data,
        }
      }else{
        return {
          status:false,
          mst:"Data not found.",
          data:[],
        }
      }
        
    }
      catch(err)
      {
        return {
          status:false,
          mst:"Something wrong with request.",
          data:[],
        }
      }
   }

exports.update = async(req)=>{
  console.log('Here i am-- Service getAll')
  // try{
    var enroll = await model.update(req.body,{
      where: { id: req.params.id }
    })
    var document = await db.document.all(req.body,{
      where: { id: req.params.id }
    })
    return enrollq
    var mappingData = []
    req.files.forEach(async(element,key) => {
      var document = {
        'title':element.filename,
        'path':element.filename,
        'type':element.mimetype, 
      }
      var adhaar = await db.document.create(document)
      mappingData = {
        'enrollement_id':enroll.id,
        'doc_id':adhaar.id,
        'type':1,
      }
      var mapping = await db.enrollement_doc_mapping.create(mappingData)

    });
        return {
          "status":true,
          "msg":"Entery was created successfully!",
          "data":enroll
        };              
  
  // }
    
  // catch(err)
  // {
  //   return {
  //     status:false,
  //     mst:"Something wrong with request.",
  //     data:[],
  //   }
  // }
}
exports.updateStatusService = async(req)=>{
  try{
    
    var status = await db.enrollmentStatus.findOne({
      where: { enrollment_id: req.params.id }
    })
    if(status){
      const update = {
        'enrollment_status':req.body.status
      }

      var updateStatus = await db.enrollmentStatus.update(update,{
        where: { enrollment_id: req.params.id }
      })

    }else{
      const update = {
            'enrollment_id':req.params.id,
            'enrollment_status':req.body.status,
            'description':'', 
            'status':1, 
          }
          var updateStatus = await db.enrollmentStatus.create(update)
    }
    if(updateStatus != null){
      return {
        "status":true,
        "msg":"Entery was created successfully!",
        "data":updateStatus
      };
    }else{
      return {
        "status":false,
        "msg":"Something wrong with request.",
        "data":[]
      };
    }
                      
  
  }
  catch(err)
  {
    return {
      status:false,
      mst:"Something wrong with request.",
      data:[],
    }
  }
}
    exports.destroy = async(id)=>{
      console.log('Here i am-- Service getAll')
    
          try{
            const updated = await model.softDelete({ where: { id: id } })
            return updated
          }
            catch(err)
            {
                return {message: err.message };
            }
}



exports.validation = (req, res, next) => {
  console.log('----Data----',req.body)
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }
    var error;
    if (!req.body.name) {
      error = "Name"
    }else if (!req.body.outlet_id) {
      error = "Outlet ID"
    }else if (!req.body.mobile_no) {
      error = "Mobile number"
    }else if (!req.body.code) {
      error = "Code"
    }else if (!req.body.city_id) {
      error = "City ID"
    }else if (!req.body.dob) {
      error = "DOB"
    }else if (!req.body.adhaar_no) {
      error = "Adhaar number"
    }else if (!req.body.pan_no) {
      error = "Pan number"
    }else if (!req.body.program_id) {
      error = "Program ID"
    }
    if(error){
      res.status(400).send({
        status:false,
        message: error+" field is required.",
        data:[]
    });
    return;
    }
    
  next();
  
};


