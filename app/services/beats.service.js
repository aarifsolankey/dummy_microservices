const db = require("../models");
const authConfig = require("../config/auth.config");
const appConfig = require("../config/app.config");
const { ExcelUpload } = require('../services/excelUpload.service');

const model = db.beats;


const Op = db.Sequelize.Op;

exports.getAllService = async(programId)=>{
  console.log('Here i am-- Service getAll')

    return  new Promise ( resolve => {
      try{
        model.findAll({
          where:{
            status:1,
            program_id:programId
          },
        }).then(data => {
          resolve(data)
        })
      }
        catch(err)
        {
            resolve({message: err.message });
        }
    })
}


exports.createService = async(post)=>{
  return  new Promise(resolve => {
    try{
        model.create(post).then(data => {
      resolve({message: "Entery was created successfully!" });              
    });
    
    }
      
    catch(err)
    {
        resolve({message: 'Input Data was not correct' });
        console.log(err.message)
    }

})
}

const beatExist = async(beat)=>{
  const data = await db.beats.findOne({attributes:['id','beat'],where:{status:1,beat:beat }})
  return data;
}
  exports.excelUploads = async(req)=>{
    try{
      const programId = req.body.program_id
        importData = await ExcelUpload(req)
        var beatsMappings = [];
        var successed = 0;
        var failed = 0;
         importData.forEach(async(row,key)  => {
          var data =  await db.beats.findOne({attributes:['id','beat'],where:{status:1,beat:row["Beat Name"],program_id:programId }})
          if(data == null){
            createBeats =  {
              "beat":row["Beat Name"],
              "mobile_number":row["SR Mobile Number"],
              "status":1,
              "program_id":programId,
            }
           var beats = await  db.beats.create(createBeats)
            var user =  await db.user.findOne()
            var datas =  await db.beats.findOne({attributes:['id','beat'],where:{status:1,beat:row["Beat Name"],program_id:programId }})
            if(beats && user){
              beatsMapping = {
                "beat_id":beats["id"],
                "user_id":user["id"],
                "status":1,
              }
              var beats = await  db.UserBeatMapping.create(beatsMapping)
              successed++
            }
          }else{
            failed++

          }
        });
        return true;
      
      
    }
      
    catch(err)
    {
      return false
    }
}

exports.getByIdService = async(id)=>{
  console.log('Here i am-- Service getAll')

    return  new Promise ( resolve => {
      try{
        model.findOne({
          where:{
            id:id,
            status:1,
          },
        }).then(data => {
          resolve(data)
        })
      }
        catch(err)
        {
            resolve({message: err.message });
        }
    })
}

exports.update = async(id,post)=>{
  console.log('Here i am-- Service getAll')

    return  new Promise ( resolve => {
      try{
        model.update(post, {
          where: { id: id }
        }).then(data => {
          resolve(data)
        })
      }
        catch(err)
        {
            resolve({message: err.message });
        }
    })
  }
    exports.destroy = async(id)=>{
      console.log('Here i am-- Service getAll')
    
        return  new Promise ( resolve => {
          try{
            var post = {
              "status":0
            }
            model.update(post, {
              where: { id: id }
            }).then(data => {
              resolve(data)
            })
          }
            catch(err)
            {
                resolve({message: err.message });
            }
        })
}

