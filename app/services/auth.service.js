const db = require("../models");
const authConfig = require("../config/auth.config");
const appConfig = require("../config/app.config");

const User = db.user;
const Profile = db.profile;

const Op = db.Sequelize.Op;
var bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");

exports.signupService = async(userData)=>{

    return  new Promise(resolve => {
        try{
            User.create({
                username: userData.username,
                phone: userData.phone,
                password: bcrypt.hashSync(userData.password, 8)
            })
            .then(user => {
                console.log(user)
              if (userData.roles) {
                Profile.findAll({
                  where: {
                    pid: {
                      [Op.or]: userData.profiles
                    }
                  }
                }).then(profiles => {
                  user.setProfiles(profiles).then(() => {
                    resolve({message: "User was registered successfully!" });              });
                });
              } else {
                // user role = 1
                user.setProfiles([1]).then(() => {
                    resolve({message: "User was registered successfully!" });
                });
              }
            })
            .catch(err => {
                resolve({message: err.message });
            });
        }
        catch(err)
        {
            resolve({message: err.message });
        }

    })
}




exports.signinService = async(userData)=>{
console.log('---dada--',userData)
  return  new Promise(resolve => {
      try{

  User.findOne({
    where: {
      username: userData.username
    }
  })
    .then(user => {
      if (!user) {
        resolve(
          {
            "status": false,
            "msg":"User Not found.",
            data:[]
          }
          );        
       
      }

      var passwordIsValid = bcrypt.compareSync(
        userData.password,
        user.password
      );

      if (!passwordIsValid) {
        resolve(
          {
            "status": false,
            "msg":"Invalid password",
            data:[]
          }
          );
      }

      var token = jwt.sign({ id: user.id }, authConfig.secret, {
        expiresIn: appConfig.expiresIn 
      });

      var authorities = [];
      user.getProfiles().then(profiles => {
        for (let i = 0; i < profiles.length; i++) {
          authorities.push({pid:profiles[i].pid,name:profiles[i].name,code:profiles[i].code});
        }
        resolve({
          id: user.id,
          username: user.username,
          phone: user.phone,
          profiles: authorities,
          accessToken: token
        });
      });
    })
    .catch(err => {

      resolve(
        {
          "status": false,
          "msg":"Something wrong with request.",
          data:err
        }
        );
    });
         
      }
      catch(err)
      {
        resolve(
          {
            "status": false,
            "msg":"Something wrong with request.",
            data:err
          }
          );
      }

  })
}

