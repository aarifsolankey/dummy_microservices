const db = require("../models");
const authConfig = require("../config/auth.config");
const appConfig = require("../config/app.config");

const model = db.comment;


const Op = db.Sequelize.Op;

exports.getAllService = async(programId)=>{
  console.log('Here i am-- Service getAll')

    return  new Promise ( resolve => {
      try{
        model.findAll({
          where:{
            status:1,
            program_id:programId
          },
        }).then(data => {
          resolve(data)
        })
      }
        catch(err)
        {
            resolve({message: err.message });
        }
    })
}


exports.createService = async(post)=>{
  return  new Promise(resolve => {
    try{
        model.create(post).then(data => {
      resolve({message: "Entery was created successfully!" });              
    });
    
    }
      
    catch(err)
    {
        resolve({message: 'Input Data was not correct' });
        console.log(err.message)
    }

})
}

exports.getByIdService = async(id)=>{
  console.log('Here i am-- Service getAll')

    return  new Promise ( resolve => {
      try{
        model.findOne({
          where:{
            id:id,
            status:1,
          },
        }).then(data => {
          resolve(data)
        })
      }
        catch(err)
        {
            resolve({message: err.message });
        }
    })
}

exports.update = async(id,post)=>{
  console.log('Here i am-- Service getAll')

    return  new Promise ( resolve => {
      try{
        model.update(post, {
          where: { id: id }
        }).then(data => {
          resolve(data)
        })
      }
        catch(err)
        {
            resolve({message: err.message });
        }
    })
  }
    exports.destroy = async(id)=>{
      console.log('Here i am-- Service getAll')
    
        return  new Promise ( resolve => {
          try{
            var post = {
              "status":0
            }
            model.update(post, {
              where: { id: id }
            }).then(data => {
              resolve(data)
            })
          }
            catch(err)
            {
                resolve({message: err.message });
            }
        })
}

