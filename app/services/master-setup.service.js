const db = require("../models");
const authConfig = require("../config/auth.config");
const appConfig = require("../config/app.config");
var bcrypt = require("bcryptjs");
const { state } = require("../models");

const model = db.program;


const Op = db.Sequelize.Op;

exports.uploadExcel = async(programId)=>{

}
  exports.DeleteMasterSetupService = async(programId)=>{
      if(!programId){
        return "program id is required"
      }
      await db.sequelize.query("DELETE FROM programs where id="+programId)
      await db.sequelize.query("DELETE FROM regions where program_id="+programId)
      await db.sequelize.query("DELETE FROM states where program_id="+programId)
      await db.sequelize.query("DELETE FROM cities where program_id="+programId)
      await db.sequelize.query("DELETE FROM channels where program_id="+programId)
      await db.sequelize.query("DELETE FROM channel_classifications where id IN(SELECT channel_classification_id FROM channels where program_id="+programId+")")
      await db.sequelize.query("DELETE FROM units where program_id="+programId)
      await db.sequelize.query("DELETE FROM frequencies where program_id="+programId)
      await db.sequelize.query("DELETE FROM languages where program_id="+programId)
      await db.sequelize.query("DELETE FROM challenge_reasons where program_id="+programId)
      await db.sequelize.query("DELETE FROM brands where brand_category_id IN (SELECT id FROM brand_categories where program_id="+programId+")")
      await db.sequelize.query("DELETE FROM brand_categories where program_id="+programId)
      await db.sequelize.query("DELETE FROM beats where program_id="+programId)
    return "All Data deleted"   
  }

    exports.MasterSetupService = async(programId)=>{
    var response = [];
    response.push("Cadbury Created successfully")
    response.push( await this.createHubadmin(programId))
    response.push( await this.createRegion(programId,"South"))
    response.push( await this.createRegion(programId,"North"))
    response.push( await this.createRegion(programId,"West"))
    response.push( await this.createRegion(programId,"East"))
    response.push( await this.createCities(programId))
    response.push( await this.createChannel(programId))
    response.push( await this.createUnits(programId,"Kg"))
    response.push( await this.createUnits(programId,"Ltr"))
    response.push( await this.createFrequency(programId,"Monthly"))
    response.push( await this.createLanguage(programId))
    response.push( await this.createChallengeReason(programId))
    response.push( await this.createBrands(programId))
    response.push( await this.createBeats(programId))
    return response
}


exports.createBeats = async(programId)=>{
  var createBeat = {
    "beat":"All",
    "status":1,
    "program_id":programId,
  }
  var create = await db.beats.create(createBeat)
  if(create){
    return "Beats created successfully"
  }else{
    return "Something wrong in beats creation"
  }
}
  

exports.createBrands = async(programId)=>{
  var formClass = {
    "brand_category_name":"Chocolate",
    "crown_brand_name":"choco",
    "short_name":1,
    "program_id":programId,
  }
  
  var brandCategory = await db.brandCategory.create(formClass)
  var getBrand = await this.getBrand(programId,brandCategory.id)
  var  createBrand = await db.brand.bulkCreate(getBrand)
  if(brandCategory && createBrand){
    return "Brand category and brands created successfully"
  }else{
    return "Something wrong in Brand category and brands creation"
  }
}

exports.getBrand = (programId,categoryId)=>{
    
  var data = [
      {"par_brand_id":"CDM 13.2G x 56U FLOWFACK","brand_name":"CDM 13.2G x 56U FLOWFACK","brand_desc":"CDM 13.2G x 56U FLOWFACK","status":1,"brand_category_id":categoryId,"program_id":programId},
      {"par_brand_id":"FERK 13G FLOWPACK x 48U","brand_name":"FERK 13G FLOWPACK x 48U","brand_desc":"FERK 13G FLOWPACK x 48U","status":1,"brand_category_id":categoryId,"program_id":programId},
      {"par_brand_id":"BOURNVITA 500 G JAR.","brand_name":"BOURNVITA 500 G JAR.","brand_desc":"BOURNVITA 500 G JAR.","status":1,"brand_category_id":categoryId,"program_id":programId},
      {"par_brand_id":"PERK 13G FLOWPACK x 30U","brand_name":"PERK 13G FLOWPACK x 30U","brand_desc":"PERK 13G FLOWPACK x 30U","status":1,"brand_category_id":categoryId,"program_id":programId}
     

  ]
  return data;

}



exports.createChallengeReason = async(programId)=>{
  var ReasonData = await this.getChallangeReason(programId)
  var  createReason = await db.challengeReason.bulkCreate(ReasonData)

  if(createReason){
    return "Challange reasons created successfully"
  }else{
    return "Something wrong in languages creation"
  }
}
  exports.getChallangeReason = (programId)=>{
    
    var data = [
        {"reason":"This is my Correct mobile number, please update.","status":1,"program_id":programId},
        {"reason":"This is my correct Owner name, please update.","status":1,"program_id":programId},
        {"reason":"This is my correct Outlet name, please update.","status":1,"program_id":programId},
        {"reason":"All the data entered is correct, please update.","status":1,"program_id":programId}

    ]
    return data;

}






exports.createLanguage = async(programId)=>{
  var languages = await this.getlanguages(programId)
  var  createLang = await db.language.bulkCreate(languages)

  if(createLang){
    return "languages created successfully"
  }else{
    return "Something wrong in languages creation"
  }
}
  exports.getlanguages = async(programId)=>{
    
    var languages = [
        {"short_name":"en","lang_name":"English","status":1,"program_id":programId},
        {"short_name":"tm","lang_name":"Tamil","status":1,"program_id":programId},
        {"short_name":"kn","lang_name":"Kannada","status":1,"program_id":programId},
        {"short_name":"my","lang_name":"Malayalam","status":1,"program_id":programId}

    ]
    return languages;

}

  exports.createFrequency = async(programId,name)=>{
  var frequencyCreate = {
    "campaign_id":0,
    "frequency_name":name,
    "program_id":programId,
    "status":1,
  }
  var query = await db.frequency.create(frequencyCreate)
  if(query){
    return name + " frequency created successfully"
  }else{
    return "Something wrong in units creation"
  }
}

  exports.createUnits = async(programId,unitName)=>{
  var unit = {
    "unit_name":unitName,
    "unit_desc":unitName,
    "unit_value":1,
    "program_id":programId,
    "status":1,
  }
  var unitCreate = await db.unit.create(unit)
  if(unitCreate){
    return unitName + " Unit created successfully"
  }else{
    return "Something wrong in units creation"
  }
}

  exports.createChannel = async(programId)=>{
  var formClass = {
    "classification_name":"Store",
    "status":1,
  }
  
  var classfication = await db.channel_classification.create(formClass)
  var channelForm = {
    "channel_code":"Store",
    "channel_desc":"General",
    "parent_channel_id":0,
    "hierarchy_level":0,
    "channel_classification_id":classfication.id,
    "program_id":programId,
    "status":1,
  }
  var channel = await db.channel.create(channelForm)
  if(classfication && channel){
    return "Channel classification and channel created successfully"
  }else{
    return "Something wrong in channel classification and channel creation"
  }
}

  exports.createCities = async(programId)=>{
  var states = await db.state.findAll({where:{program_id:programId}});
  var stateData = [];
  states.forEach(async(state,key) => { 
    var state = await this.getCities(programId,state['id'],state['state_name'])
    stateData[key] = db.city.bulkCreate(state)
  }); 
  return "Cities created successfully." 
}
exports.getCities = async(programId,stateId,stateName)=>{
  var cities =  {
    "Uttarakhand":[
        {"cityname" : "Dehradun","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Haridwar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Roorkee","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Haldwani","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rudrapur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kashipur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rishikesh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Pithoragarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ramnagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kichha","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Manglaur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jaspur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kotdwara","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nainital","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Almora","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mussoorie","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sitarganj","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bazpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Pauri","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tehri","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nagla","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Laksar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chamoli Gopeshwar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Umru Khurd","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Srinagar","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Andhra Pradesh" :[
        {"cityname" : "Visakhapatnam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Vijayawada","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Guntur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nellore","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kurnool","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kakinada","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rajamahendravaram","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kadapa","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tirupati","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Anantapuram","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Vizianagaram","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Eluru","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nandyal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ongole","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Adoni","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Madanapalle","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Machilipatnam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tenali","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Proddatur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chittoor","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhimavaram","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Hindupur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Srikakulam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gudivada","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Guntakal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tadepalligudem","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dharmavaram","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Narasaraopet","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tadipatri","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mangalagiri","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Amaravati","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "MADHYA PRADESH" :[
        {"cityname" : "Indore","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhopal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jabalpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gwalior","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ujjain","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dewas","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Satna","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ratlam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rewa","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Murwara","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Singrauli","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Burhanpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Khandwa","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhind","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chhindwara","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Guna","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Shivpuri","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Vidisha","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chhatarpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Damoh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mandsaur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Khargone","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Neemuch","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Pithampur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gadarwara","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Hoshangabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Itarsi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sehore","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Morena","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Betul","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Seoni","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Datia","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nagda","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nagda","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Tamil Nadu" :[
        {"cityname" : "Chennai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Coimbatore","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Madurai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tiruchirappalli","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tiruppur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Salem","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Erode","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tirunelveli","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Vellore","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Thoothukkudi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dindigul","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Thanjavur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ranipet","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sivakasi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Karur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Udhagamandalam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Hosur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nagercoil","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kanchipuram","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kumarapalayam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Karaikkudi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Neyveli","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Cuddalore","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kumbakonam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tiruvannamalai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Pollachi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rajapalayam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gudiyatham","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Pudukkottai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Vaniyambadi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ambur","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Telangana" :[
        {"cityname" : "Hyderabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Warangal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nizamabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Khammam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Karimnagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ramagundam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mahabubnagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nalgonda","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Adilabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Siddipet","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Miryalaguda","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Suryapet","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jagtial","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Kerala" :[
        {"cityname" : "Thiruvananthapuram","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kozhikode","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kochi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kollam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Thrissur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kannur","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Maharashtra" :[
        {"cityname" : "Mumbai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "PMC, Pune","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nagpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Thane","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "PCMC, Pune","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nashik","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kalyan-Dombivli","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Vasai-Virar city MC","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Aurangabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Navi Mumbai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Solapur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mira-Bhayandar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Latur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Amravati","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nanded Waghala","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kolhapur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Akola","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Panvel","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ulhasnagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sangli-Miraj-Kupwad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Malegaon","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jalgaon","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dule","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhivandi-Nizampur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ahmednagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chandrapur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Parbhani","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ichalkaranji","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jalna","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ambarnath","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhusawal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ratnagiri","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Beed","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gondia","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Satara","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Barshi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Yavatmal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Achalpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Osmanabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nandurbar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Wardha","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Udgir","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Gujarat" :[
        {"cityname" : "Ahmedabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Surat","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Vadodara","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rajkot","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhavnagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jamnagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Junagadh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gand­hi­na­gar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gandhidham","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Anand","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Navsari","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Morbi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nadiad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Surendranagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bharuch","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mehsana","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhuj","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Porbandar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Palanpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Valsad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Vapi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gondal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Veraval","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Godhara","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Patan","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kalol","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gandhinagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dahod","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Botad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Amreli","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Deesa","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jetpur","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Goa" :[
        {"cityname" : "Bicholim","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Canacona","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Cuncolim","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Curchorem","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mapusa","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Margao","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mormugao","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "PanajiCapital","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Pernem","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ponda","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Quepem","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sanguem","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sanquelim","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Valpoi","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Odisha" :[
        {"cityname" : "Bhubaneswar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Cuttack","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Raurkela","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Brahmapur (Berhampur)","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sambalpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Puri","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Baleshwar (Balasore)","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhadrak","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Baripada","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Balangir","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jharsuguda","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jaypur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bargarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Brajarajnagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rayagada","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhawanipatna","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Paradip","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dhenkanal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Barbil (Bada Barabil)","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jatani","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kendujhar (Kendujhargarh)","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Byasanagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rajagangapur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sunabeda","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Koraput","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Bihar" :[
        {"cityname" : "Patna","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gaya","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhagalpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Muzaffarpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Purnia","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Darbhanga","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bihar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Arrah","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Begusarai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Katihar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Munger","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chhapra","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Danapur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bettiah","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Saharsa","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sasaram","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Hajipur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dehri","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Siwan","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Motihari","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nawada","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bagaha","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Buxar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kishanganj","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sitamarhi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jamalpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jehanabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Aurangabad","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "West Bengal" :[
            {"cityname" : "Kolkata","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Howrah","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Darjeeling","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Kalimpong","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Suri","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Sainthia","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Bolpur","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Kharagpur","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Bardhaman","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Asansol","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Durgapur","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Murshidabad","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Malda City","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Siliguri","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Jalpaiguri","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Raiganj","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Balurghat","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Purulia","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Baharampur","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Krishnanagar","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Barasat","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Barrackpore","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Ranaghat","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Baranagar","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Serampore","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Chandannagar","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Chinsura","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Kalyani","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Tamluk","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Medinipur","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Nabadwip","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Contai","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Cooch Behar","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Bankura","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Bishnupur","state_id" : stateId,"program_id" : programId,"status" : 1},
            {"cityname" : "Haldia","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Jharkhand" :[
        {"cityname" : "Jamshedpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dhanbad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ranchi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bokaro Stee","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Deoghar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chakradharp","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Phusro","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Hazaribagh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Giridih","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ramgarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Medininagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chirkunda","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Manipur" :[
        {"cityname" : "Bishnupur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chandel","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Churachandpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Imphal East","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Imphal West","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Senapati","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tamenglong","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Thoubal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ukhrul","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Meghalaya" :[
        {"cityname" : "East Garo Hills","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "East Khasi Hills","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jaintia Hills","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ri Bhoi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Shillong","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "South Garo Hills","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "West Garo Hills","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "West Khasi Hills","state_id" : stateId,"program_id" : programId,"status" : 1}

    ],
    "Mizoram" :[
        {"cityname" : "Aizawl","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Champhai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kolasib","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Lawngtlai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Lunglei","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mamit","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Saiha","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Serchhip","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Tripura" :[
        {"cityname" : "Agartala","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ambasa","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bampurbari","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Belonia","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dhalai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dharam Nagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kailashahar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kamal Krishnabari","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Khopaiyapara","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Khowai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Phuldungsei","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Radha Kishore Pur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tripura","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Rajasthan" :[
        {"cityname" : "Jaipur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jodhpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kota","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhiwadi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bikaner","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Udaipur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ajmer","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhilwara","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Alwar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sikar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bharatpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Pali","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sri Ganganagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kishangarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Baran","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dhaulpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tonk","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Beawar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Hanumangarh","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Arunachal Pradesh" :[
        {"cityname" : "Itanagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bomdila","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Deomali","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dirang","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rupa","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tawang","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Yingkiong","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ziro","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mechuka","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Assam" :[
        {"cityname" : "Guwahati","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Silchar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dibrugarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jorhat","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bongaigaon","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tinsukia","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tezpur","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Ladakh" :[{}],
    "Himachal Pradesh" :[
        {"cityname" : "Shimla","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Solan","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dharamsala","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Baddi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nahan","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mandi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Paonta Sahib","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sundarnagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chamba","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Una","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kullu","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Hamirpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bilaspur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Yol Cantonment","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nalagarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nurpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kangra","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Santokhgarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mehatpur Basdehra","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Shamshi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Parwanoo","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Manali","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tira Sujanpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ghumarwin","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dalhousie","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rohru","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nagrota Bagwan","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rampur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kumarsain","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jawalamukhi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jogindernagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dera Gopipur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sarkaghat","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jhakhri","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Indora","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhuntar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nadaun","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Theog","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kasauli Cantonment","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gagret","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chuari Khas","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Daulatpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sabathu Cantonment","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dalhousie Cantonment","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Palampur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rajgarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Arki","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dagshai Cantonment","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Seoni","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Talai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jutogh Cantonment","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chaupal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rewalsar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bakloh Cantonment","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jubbal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhota","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Banjar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Naina Devi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kotkhai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Narkanda","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Uttar Pradesh" :[
        {"cityname" : "Lucknow","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kanpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ghaziabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Agra","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Meerut","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Varanasi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Allahabad (Prayagraj)","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bareilly","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Aligarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Moradabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Saharanpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gorakhpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Faizabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Firozabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jhansi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Muzaffarnagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mathura-Vrindavan","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Budaun","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rampur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Shahjahanpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Farrukhabad-cum-Fategarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ayodhya Cantt","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Maunath Bhanjan","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Hapur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Noida","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Etawah","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mirzapur-cum-Vindhyachal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bulandshahr","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sambhal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Amroha","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Hardoi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Fatehpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Raebareli","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Orai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sitapur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bahraich","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Modinagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Unnao","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jaunpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Lakhimpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Hathras","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Banda","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Pilibhit","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mughalsarai","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Barabanki","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Khurja","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gonda","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mainpuri","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Lalitpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Etah","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Deoria","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ujhani","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ghazipur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sultanpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Azamgarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bijnor","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sahaswan","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Basti","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chandausi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Akbarpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ballia","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tanda","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Greater Noida","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Shikohabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Shamli","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Awagarh Etah","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kasganj","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Punjab" :[
        {"cityname" : "Ludhiana","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Amritsar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jalandhar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Patiala","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bathinda","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ajitgarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Hoshiarpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Moga","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Pathankot","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Batala","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Abohar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Maler Kotla","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Khanna","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Phagwara","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Muktsar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Barnala","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Firozpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kapurthala","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Zirakpur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rajpura","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kot Kapura","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sangrur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Faridkot","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mansa","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gobindgarh","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Haryana" :[
        {"cityname" : "Faridabad","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gurugram","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Panipat","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Ambala","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Yamunanagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rohtak","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Hisar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Karnal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sonipat","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Panchkula","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhiwani","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sirsa","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bahadurgarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jind","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Thanesar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kaithal","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rewari","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mahendergarh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Pundri","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kosli","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Jammu and Kashmir" :[
        {"cityname" : "Anantnag","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Baramulla","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Budgam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Doda","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Jammu","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kargil","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kathua","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kupwara","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Leh","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Poonch","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Pulwama","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rajauri","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Srinagar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Udhampur","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Nagaland" :[
        {"cityname" : "Dimapur","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kohima","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mokokchung","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mon","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Phek","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tuensang","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Wokha","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Zunheboto","state_id" : stateId,"program_id" : programId,"status" : 1}

    ],
    "Sikkim" :[
        {"cityname" : "Be","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Bhurtuk","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chidam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chubha","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Chumikteng","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dentam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dikchu","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Dzongri","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gangtok","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gauzing","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Gyalshing","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Hema","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Kerung","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Lachen","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Lachung","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Lema","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Lingtam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Lungthu","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Mangan","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Namchi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Namthang","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nanga","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Nantang","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Naya Bazar","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Padamachen","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Pakhyong","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Pemayangtse","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Phensang","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rangli","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Rinchingpong","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sakyong","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Samdong","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Singtam","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Siniolchu","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sombari","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Soreng","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Sosing","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tekhug","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Temi","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tsetang","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tsomgo","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Tumlong","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Yangang","state_id" : stateId,"program_id" : programId,"status" : 1},
        {"cityname" : "Yumtang","state_id" : stateId,"program_id" : programId,"status" : 1}
    ],
    "Delhi" :[{}],
    "Chandigarh" :[{}],
    "CHATTISGARH" :[
    {"cityname" : "Raipur","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Bhilai (Bhilai Nagar)","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Bilaspur","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Korba","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Raj Nandgaon","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Raigarh","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Jagdalpur","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Ambikapur","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Dhamtari","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Chirmiri","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Bhatapara","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Mahasamund","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Dalli-Rajhara","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Kawardha","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Champa","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Naila Janjgir","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Kanker","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Dongragarh","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Tilda","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Mungeli","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Manendragarh","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Kondagaon","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Gobranawapara","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Bemetara","state_id" : stateId,"program_id" : programId,"status" : 1},
    {"cityname" : "Baikunthpur","state_id" : stateId,"program_id" : programId,"status" : 1}
    ]
}
return cities[stateName]
}
  exports.createRegion = async(programId,region)=>{
  var form = {
    "region_name":region,
    "status":1,
    "geographical_id":0,
    "program_id":programId
  }
  var createRegion = await db.region.create(form)
  var id = createRegion.id;
  if(region == "East"){
    createState = await this.getEastState(programId,id)
    var state = db.state.bulkCreate(createState)
  }else if(region == "South"){
    createState = await this.getSouthState(programId,id)
    var state = db.state.bulkCreate(createState)
  }else if(region == "North"){
    createState = await this.getNorthState(programId,id)
    var state = db.state.bulkCreate(createState)
  }else if(region == "West"){
    createState = await this.getWestState(programId,id)
    var state = db.state.bulkCreate(createState)
  }
  return region+" and all the states for "+region+" Created successfully"

  return region +" created successfully."
}

  exports.getNorthState = async(programId,region)=>{
  var date = new Date();
  var random = Math.floor(1000 + Math.random() * 9000);
  states = [
    {
      'state_name':'Rajasthan',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Uttar Pradesh',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Arunachal Pradesh',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Assam',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Ladakh',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Himachal Pradesh',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Uttarakhand',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Punjab',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Haryana',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Jammu and Kashmir',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Meghalaya',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Manipur',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Mizoram',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Nagaland',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Tripura',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Sikkim',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Delhi',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Chandigarh',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'MADHYA PRADESH',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'CHATTISGARH',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
  ]
  return states
}
  exports.getEastState = async(programId,region)=>{
  var date = new Date();
  var random = Math.floor(1000 + Math.random() * 9000);
  states = [
    {
      'state_name':'Odisha',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Bihar',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'West Bengal',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Jharkhand',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Arunachal Pradesh',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Assam',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Manipur',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Meghalaya',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Mizoram',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Nagaland',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Sikkim',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      {
      'state_name':'Tripura',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId
      },
      
  ]
  return states
}

  exports.getWestState = async(programId,region)=>{
  var date = new Date();
  var random = Math.floor(1000 + Math.random() * 9000);
      var states =  [
        {
          'state_name':'Maharashtra',
          "region_id":region,
          "state_code":random,
          "status":1,
          "program_id":programId,
        },
        {
          'state_name':'Gujarat',
          "region_id":region,
          "state_code":random,
          "status":1,
          "program_id":programId,
        },
        {
          'state_name':'Goa',
          "region_id":region,
          "state_code":random,
          "status":1,
          "program_id":programId,
        },
        {
          'state_name':'Andhra Pradesh',
          "region_id":region,
          "state_code":random,
          "status":1,
          "program_id":programId,
        }
    ];
  return states;
}
  exports.getSouthState = async(programId,region)=>{
  var date = new Date();
  var random = Math.floor(1000 + Math.random() * 9000);

  var states =  [
    {
      'state_name':'Karnataka',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId,
    },
    {
      'state_name':'Tamil Nadu',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId,
    },
    {
      'state_name':'Telangana',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId,
    },
    {
      'state_name':'Kerala',
      "region_id":region,
      "state_code":random,
      "status":1,
      "program_id":programId,
    }
];
return states;
}
function bulkInsert(connection, table, objectArray, callback) {
  let keys = Object.keys(objectArray[0]);
  let values = objectArray.map( obj => keys.map( key => obj[key]));
  let sql = 'INSERT INTO ' + table + ' (' + keys.join(',') + ') VALUES ?';
  connection.query(sql, [values], function (error, results, fields) {
    if (error) callback(error);
    callback(null, results);
  });
}

  exports.createHubadmin = async(programId)=>{
  
  checkUserExist = await db.user.findOne(
  {
  attributes: ['id', 'username'],
  where:{username:"Hubadmin"}
  })
  if(checkUserExist == null){
    var pass = bcrypt.hashSync("123456", 8);
    var formData = {
      "username":"Hubadmin",
      "email":"hubadmin@gamil.com",
      "password":pass,
      "program_id":programId,
    }
    create = await db.user.create(formData)
    return "Hubadmin created successfully.";
  }else{
    var updateData = {
      "program_id":programId,
    }
    update = await db.user.update(updateData,{
    where: { id: checkUserExist.id }
    })
    return "Hubadmin Already exist programId updated successfully."
  }
  
}