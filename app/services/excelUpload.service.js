const db = require("../models");
const authConfig = require("../config/auth.config");
const appConfig = require("../config/app.config");
const { state } = require("../models");
const readXlsxFile = require("read-excel-file/node");

  exports.ExcelUpload = async (req,fields) => {
      if (req.file == undefined) {
        return "Please upload an excel file!";
      }
      let path = "upload/" + req.file.filename;
      const import_data = await readXlsxFile(path)
      // import_data.shift();
      var data = toKeyedRows(import_data)
      return data;
  };

  const toKeyedRows = (data) => {
    // return data[0];
    const titles = data[0];
    return data.slice(1).map(row => Object.fromEntries(row.map((value, i) => ([titles[i], value])))
    );
  }
  

  function beatExist(beats) {
    var checkUser =  db.beats.findOne({ where:{status:1,beat:beats}})
    return checkUser;
  }
    exports.ExcelUploadBeats = async (req) => {
    if (req.file == undefined) {
      return "Please upload an excel file!";
    }
    
    
    let path = "upload/" + req.file.filename;
    const import_data = await readXlsxFile(path)
    import_data.shift();
      var results = [];
      import_data.forEach(async(row,key)  => {
        let checkUser = await beatExist("Alls8");
        if(!checkUser){
            result = {
            "beat":row[0],
            "mobile_number":row[1],
            "status":1,
            "program_id":req.body.program_id,
            }
            results.push(result);
            success.push({key});
          }else{
            result = {
              "beat":row[0],
              "mobile_number":row[1],
              "status":1,
              "program_id":req.body.program_id,
              }
            results.push(checkUser);
          failed.push({key});
          }
      });
      return [{"success":success,"failed":failed,"result":results}]
      db.beats.bulkCreate(results)

      return results;
};