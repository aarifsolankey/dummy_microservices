const fs = require("fs");

exports.crud = (req) => {
    // Create controller
    var response = [];
    
    if(req.body.controller && req.body.controller == true){
    fs.readFile("./crud/content/controller.js", "utf-8", (err, data) => {
        console.log('------daaaaa----', data);
        var controllerData = data.replace('__SERVICE___', req.body.crud);
        fs.writeFile('./app/controllers/' + req.body.crud + '.controller.js', controllerData, (err, res) => {
            if (err) console.log('------', err);
            // Create Service
            fs.readFile("./crud/content/service.js", "utf-8", (err, service) => {
                console.log('------daaaaa----', service);
                var serviceFile = service.replace('___MODEL___', req.body.crud);
                fs.writeFile('./app/services/' + req.body.crud + '.service.js', serviceFile, (err, res) => {
                    if (err) console.log('------', err);
                });
            // Create Route
                    fs.readFile("./crud/content/route.js", "utf-8", (err, route) => {
                        var routeFile = route.split('__ROUTE__').join(req.body.crud);
                        fs.writeFile('./app/routes/' + req.body.crud + '.route.js', routeFile, (err, res) => {
                            if (err) console.log('------', err);
                            // Add route in service.js
                            fs.readFile("./crud/content/servercontent.js", "utf-8", (err, serverContent) => {
                                fs.readFile("./server.js", "utf-8", (err, server) => {
                                    var serverContentFile = serverContent.replace('__ROUTE__', req.body.crud);
                                    var serverFile = server.replace('//__ADD_NEW_ROUTE__', serverContentFile);
                                    fs.writeFile('./server.js', serverFile, (err, res) => {
                                        if (err) console.log('------', err);
                                    });
                                });
                            });
                        });
                    });
            });
        });
    });
    response.push(req.body.crud+" Controller genrated successfully");
    response.push(req.body.crud+" service genrated successfully");
    response.push(req.body.crud+" route genrated successfully");
    response.push(req.body.crud+" route configrate successfully in server file");
}
if(req.body.model && req.body.model == true){
    // Create model
    fs.readFile("./crud/content/model.js", "utf-8", (err, data) => {
        var fields = req.body.fields
        var fieldsString = JSON.stringify(fields)
        var dataTypes = fieldsString.split('___').join('DataType.');
        var removeCots = dataTypes.split(/['"]+/g).join('');
        var removeCotsF = removeCots.split('[').join('');
        var removeCotsL = removeCotsF.split(']').join('');
        var removeTrash = data.split('______FIELDS').join(removeCotsL);
        var modelContent = removeTrash.replace('_TABLE_', req.body.table);
        fs.writeFile('./app/models/' + req.body.crud + '.model.js', modelContent, (err, res) => {
            if (err) console.log('------', err);
            // Add model in index
            fs.readFile("./crud/content/indexcontent.js", "utf-8", (err, indexContent) => {
                fs.readFile("./app/models/index.js", "utf-8", (err, indexFileData) => {
                    var indexContentFile = indexContent.split('__MODEL___').join(req.body.crud);
                    var indeServerFile = indexFileData.replace('// __ADD_NEW_MODEL__', indexContentFile);
                    fs.writeFile('./app/models/index.js', indeServerFile, (err, res) => {
                        if (err) console.log('------', err);
                    });
                });
            });
           
        });
    });
    response.push(req.body.crud+" model genrated successfully");
    response.push(req.body.crud+" model configrate successfully in index file");
}
if(!req.body.model){
    response.push("Please make model true if you wanna create model");

}
if(!req.body.controller){
    response.push("Please make controller true if you wanna create controller");

}
return response;
}

