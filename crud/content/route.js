module.exports = app => {
    const controller = require("../controllers/__ROUTE__.controller.js");
    var router = require("express").Router();
    const { authJwt } = require("../middleware");

    router.post("/", controller.create);
    router.get("/list/:programId",[authJwt.verifyToken, authJwt.isAdmin], controller.getAll);
    router.get("/:id",[authJwt.verifyToken, authJwt.isAdmin],  controller.findOne);
    router.put("/:id",[authJwt.verifyToken, authJwt.isAdmin],  controller.edit);
    router.delete("/:id",[authJwt.verifyToken, authJwt.isAdmin],  controller.delete);
    app.use('/api/__ROUTE__', router);
};