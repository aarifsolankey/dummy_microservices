const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
require('dotenv').config();

var corsOptions = {
  origin: "http://localhost:4200"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");
const Profile = db.profile;

if(false)
db.sequelize.sync({force: true}).then(() => {
  console.log('Drop and Resync Db');
  initial();
});

function initial() {
  Profile.create({
      id: 5,
      name: "Retailer",
      code: "RTLR",
      pid:555
    });
   
    Profile.create({
      id: 2,
      name: "Agent",
      code: "AGNT",
      pid:222

    });
   
    Profile.create({
      id: 3,
      name: "Sales",
      code: "SALE",
      pid:333
    });
    Profile.create({
      id: 4,
      name: "Distributor",
      code: "DSTR",
      pid:444
    });
    Profile.create({
      id: 1,
      name: "Admin",
      code: "ADMN",
      pid:111
    });
    Profile.create({
      id: 6,
      name: "Manager",
      code: "MNGR",
      pid:666

    });
    Profile.create({
      id: 7,
      name: "Team Lead",
      code: "LEAD",
      pid:777
    });
    Profile.create({
      id: 8,
      name: "Clerk",
      code: "CLRK",
      pid:888

    });
    Profile.create({
      id: 9,
      name: "Mystry Shopper",
      code: "MSSH",
      pid:999
    });
  }


// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to Ali Agah application." });
});


// routes
require('./app/routes/auth.route')(app);
require('./app/routes/user.route')(app);
require('./app/routes/program.route')(app);
require('./app/routes/region.route')(app);
require('./app/routes/state.route')(app);
require('./app/routes/city.route')(app);
require('./app/routes/channel.route')(app);
require('./app/routes/language.route')(app);
require('./app/routes/brand.category.route')(app);
require('./app/routes/unit.route')(app);
require('./app/routes/frequency.route')(app);
require('./app/routes/brand.route')(app);
require('./app/routes/geographic.route')(app);
require('./app/routes/aplication.config.route')(app);
require('./app/routes/slab.route')(app);
require('./app/routes/game.route')(app);
require('./app/routes/user.role.route')(app);
require('./app/routes/channel.classification.route')(app)
require('./app/routes/beats.route')(app);
require('./app/routes/userdetail.route')(app);
require('./app/routes/userdetail.route')(app);
require('./app/routes/enrollment.route')(app);
require('./app/routes/comment.route')(app);
require('./app/routes/document.route')(app);
//__ADD_NEW_ROUTE__


// set port, listen for requests
const PORT = process.env.PORT || 8020;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});






